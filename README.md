# TP3 #

## Pré-requis ##

- nodeJS version 10
- npm

## Installation ##

Pour commencer lancer les commandes ci-dessous:
```js
npm i
```
	
## Execution ##

Pour lancer le serveur, vous avez juste à effectuer la commande "node app" 
puis vous allez sur le site localhost:8080/question-"numéro de la question".html
```js
node app
localhost:8080/question-1.html
localhost:8080/question-2.html
localhost:8080/question-3.html
```
La question 8 représente la partie à vous de jouer

## Questions abordées

Nous avons effectué toutes les parties jusqu'à à vous de jouer.
De plus, pour le moment, nous avons ajouté des météorite avec couleur.

Basma Boinaidi

