const express = require('express');
const path = require('path');
// Init
const app = express();
const port = process.env.PORT || 8040;

app.use(express.static(__dirname + '/public')) // Indique que le dossier /public contient des fichiers statiques

// routes ======================================================================
app.get('/', function (req, res) {
    res.send('Vous êtes à l\'accueil');
}).get('/question/:number', function (req, res) {
    const number = req.params.number;  // Numéro de la question
    switch (number) {
        case '1':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-1.html'));
            break;
        case '2':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-2.html'));
            break;
        case '3':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-3.html'));
            break;
        case '4':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-4.html'));
            break;
        case '5':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-5.html'));
            break;
        case '6':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-6.html'));
            break;
        case '7':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-7.html'));
            break;
        case '8':                       // Si c'est la question 1
            res.sendFile(path.join(__dirname, 'views', 'question-8.html'));
            break;

        default :
            res.setHeader('Content-Type', 'text/plain');
            res.status(404).send("Cette question n'éxiste pas");
    }
});


//launch ======================================================================
app.listen(port);
console.log('Running web server on port ' + port);